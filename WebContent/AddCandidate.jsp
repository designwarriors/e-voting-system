<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import ="election.Election, java.util.ArrayList"%>


<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
  <script >
var request;  
function sendInfo()  
{  
var v=document.vinform.candidate_id.value;  
var url="list.jsp?val="+v;  
  
if(window.XMLHttpRequest){  
request=new XMLHttpRequest();  
}  
else if(window.ActiveXObject){  
request=new ActiveXObject("Microsoft.XMLHTTP");  
}  
  
try  
{  
request.onreadystatechange=getInfo;  
request.open("GET",url,true);  
request.send();  
}  
catch(e)  
{  
alert("Unable to connect to server");  
}  
}  
  
function getInfo(){  
if(request.readyState==4){  
var val=request.responseText;  
document.vinform.candidate_name.value=val;
  
}  
}  
</script>        
  
   <title>Admin | Online University Voting System</title>
</head>
<body>
<%if(request.getSession().getAttribute("Admin")==null){
response.sendRedirect("index.jsp");
}else{%>

<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li ><a href='Admin_.jsp'><span>Manage Election</span></a></li>
   <li class='active'><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul>
</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>



    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
<div id='vermenu'>
<ul>
   <li ><a href='EditCandidate.jsp'><span>Edit Candidate</span></a></li>
   <li class='active'><a href='AddCandidate.jsp'><span>Add Candidate</span></a></li>
   <li class='last'><a href='RemoveCandidate.jsp'><span>Remove Candidate</span></a></li>
   
</ul>
</div>
		</div>
		<div class="col-md-10">


<br>
<%
ArrayList<Integer> Election_id=(ArrayList<Integer>)request.getSession().getAttribute("OngoingElectionId");
if(!Election_id.isEmpty()){
%>
		

<form action="ElectionManager" method="post" name="vinform">

<table class="tg">
 <input type="hidden" value="AddCandidate.jsp" name="requestFrom"/>
 
  <tr>
    <td class="tg-yw4l">Election ID:</td>
    
    <td class="tg-yw4l"><select name="election_id">
    <%for(int i=0;i<Election_id.size();i++){ %>
    <option value="<%=Election_id.get(i)%>"><%=Election_id.get(i)%></option>
    <%} %></select></td>
    </tr>
  <tr>
    <td class="tg-yw4l">Candidate ID:</td>
    <td class="tg-yw4l"><input type="text" name="candidate_id" onblur="sendInfo()"/></td>
     </tr>
  <tr>
       <td class="tg-yw4l">Candidate Name:</td>
    <td class="tg-yw4l"><input type="text" name="candidate_name" readonly="readonly" required="required"/></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
 <td class="tg-yw4l"></td>
    </tr>
  <tr>
    <td class="tg-yw4l">Manifesto</td>
    <td class="tg-yw4l"><textarea name="manifesto" ></textarea></td>
    
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><center><input type="submit" value="Add"></center></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>

</table>
    </form>
    <%}else {%>
    <p>There are no Elections for adding the candidate!</p>
    <%} %>
</div>
		
</div>
</div>
<%} %>
    </body>
</html>







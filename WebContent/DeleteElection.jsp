<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList,election.Election"%>


<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>Admin | Online University Voting System</title>
</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li class='active'><a href='Admin_.jsp'><span>Manage Election</span></a></li>
   <li><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul>
</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
<div id='vermenu'>
<ul>
   <li ><a href='UpdateElection.jsp'><span>Update Existing</span></a></li>
   <li ><a href='Admin_.jsp'><span>Create New</span></a></li>
   <li class='active last'><a href='DeleteElection.jsp'><span>Delete Existing</span></a></li>
   
</ul>
</div>
		</div>
		<div class="col-md-10">
		

<% String id=request.getParameter("id");
ArrayList<Integer> Election_id=(ArrayList<Integer>)request.getSession().getAttribute("ElectionId");
if(!Election_id.isEmpty()){

if(id==null){
%>
<form action="ElectionManager" method="post">
<input type="hidden" value="DeleteElection.jsp" name="requestFrom"/>
<table class="tg">
  <tr>
    <td class="tg-yw4l">Election ID:</td>
    
    <td class="tg-yw4l"><select name="election_id">
    <%for(int i=0;i<Election_id.size();i++){ %>
    <option value="<%=Election_id.get(i)%>"><%=Election_id.get(i)%></option>
    <%} %></select></td>
    <td class="tg-yw4l"><input type="submit" value="Check Details"/></td>
    </tr>
    
  
  </table>
  </form>
  <%} else{
  Election election=(Election) request.getSession().getAttribute("ElectionDetailsById");
  ArrayList<String> post=(ArrayList<String>)request.getSession().getAttribute("Post"); 
	ArrayList<String> dept=(ArrayList<String>)request.getSession().getAttribute("Department");

%>
  
  <form action="ElectionManager" method="post">
<input type="hidden" value="DeleteElection.jsp" name="requestFrom">
<input type="hidden" value="DeleteElection" name="action">
<table class="tg">
  <tr>
    <td class="tg-yw4l"><p>Election ID:</td>
    <td class="tg-yw4l"><input type="text" value="<%=election.getElectionId()%>" readonly="readonly" name="election_id"/></td>
    <td class="tg-yw4l"></td>
    </tr>
    
  <tr>
  
      <td class="tg-yw4l"><p>Post:</td>
    <td class="tg-yw4l"><input type="text" readonly="readonly" value="<%=election.getPost() %>"/></td>
    <td class="tg-yw4l"><p>of</td>
    <td class="tg-yw4l"><input type="text" readonly="readonly" value="<%=election.getDepartment()%>"/></td>
    <td class="tg-yw4l"><p>for the year</td>
    <td class="tg-yw4l"><input type="text" readonly="readonly" value="<%=election.getYear()%>"/></td>
  </tr>
  <tr>
    <td class="tg-yw4l"><p>Start Date:</td>
    <td class="tg-yw4l"><input type="text" readonly="readonly" value="<%=election.getStartDate() %>" /></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p>End Date:</td>
    <td class="tg-yw4l"><input type="text" readonly="readonly" value="<%=election.getEndDate() %>" /></td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  
  <tr>
    <td class="tg-yw4l"><p>Voters:</td>
    <td class="tg-yw4l"><p>Year</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p>Department</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=election.getVoterYear() %></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=election.getVoterDepartment() %></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">
</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>

</table>
    <center><input type="submit" value="Delete"></center></form>
<%}}else{ %>
<div class="col-md-1"></div>
<div class="col-md-7">

<hr><p>     Sorry, there are no elections to delete! Thank you !<hr>
</div>

<%} %>
<script language="JavaScript">
function toggle(source) {
	  checkboxes = document.getElementsByName('voter_year');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function toggle2(source) {
	  checkboxes = document.getElementsByName('voter_department');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}</script>

</div>
		
</div>
</div>

    </body>
</html>







<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="election.Election"%>


<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
<script >
var request;  
function sendInfo()  
{  
var v=document.vinform.candidate_id.value;  
var url="list.jsp?val="+v;  
  
if(window.XMLHttpRequest){  
request=new XMLHttpRequest();  
}  
else if(window.ActiveXObject){  
request=new ActiveXObject("Microsoft.XMLHTTP");  
}  
  
try  
{  
request.onreadystatechange=getInfo;  
request.open("GET",url,true);  
request.send();  
}  
catch(e)  
{  
alert("Unable to connect to server");  
}  
}  
  
function getInfo(){  
if(request.readyState==4){  
var val=request.responseText;  
document.vinform.candidate_name.value=val;
  
}  
}  
</script>        
   <title>Admin | Online University Voting System</title>
</head>
<body>
<%if (request.getSession().getAttribute("Admin")==null)
	response.sendRedirect("index.jsp");
else{%>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li class='active'><a href='Admin_jsp'><span>Manage Election</span></a></li>
   <li><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul>
</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>




    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
<div id='vermenu'>
<ul>
   <li ><a href='UpdateElection.jsp'><span>Update Existing</span></a></li>
   <li class='active'><a href='Admin_.jsp'><span>Create New</span></a></li>
   <li class='last'><a href='DeleteElection.jsp'><span>Delete Existing</span></a></li>
   
</ul>
</div>
		</div>
		<div class="col-md-10">
		<center><ul id="design">
    <li><a  ><b>Election Details</b></a></li>
    <li><a class="current"><b>Add Candidates</b></a></li>
    <li><a ><b>Create Election</b></a></li>
    
    <li><a ><b>Post Election</b></a></li>
</ul></center><br>
<% Election election=new Election();
	election=(Election)request.getSession().getAttribute("ElectionDetails");
	if(election!=null){
		
	%>

<form name="vinform" action="ElectionManager" method="post">
<input type="hidden" value="CreateElection_3.jsp" name="nextpage"/>
<input type="hidden" value="CreateElection_2.jsp" name="requestFrom"/>

<table class="tg">
  <tr>
    <td class="tg-yw4l">Election ID:</td>
    <td class="tg-yw4l"><input type="text" name="election_id" value="<%=election.getElectionId()%>" readonly="readonly"/></td>
    </tr>
  <tr>
    <td class="tg-yw4l">Candidate ID:</td>
    <td class="tg-yw4l"><input type="text" name="candidate_id" onblur="sendInfo()"/></td>
     </tr>
  <tr>
    <td class="tg-yw4l">Candidate Name:</td>
    <td class="tg-yw4l"><input type="text" name="candidate_name"/></td>
    <td class="tg-yw4l"><span id="err"> </span></td>
    <td class="tg-yw4l"></td>
   <!-- <td class="tg-yw4l">Candidate Department:</td>
    <td class="tg-yw4l"><input type="text" name="candidate_dept"/></td>
  --> 
  </tr>
  <tr>
    <td class="tg-yw4l">Manifesto</td>
    <td class="tg-yw4l"><textarea name="manifesto" ></textarea></td>
    
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><center><input type="submit" value="Add This Candidate"></center></td></form>
   <form action="ElectionManager" method="post">
    <td class="tg-yw4l"><input type="hidden" value="ProceedToSummary" name="requestFrom"/></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><center><input type="submit" value="Proceed"></center></a></td>
    <td class="tg-yw4l"></td>
  </tr>

</table>
</script>


</div>
		
</div>
</div>
<%}else{
	response.sendRedirect("Admin_.jsp");
}
		} %>
    </body>
</html>







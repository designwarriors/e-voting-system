<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList"%>


<head>
     <%@include file="stylesheet.jsp" %>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script>
	function DateCheck()
	{
	  var StartDate= document.admin.start_date.value;
	  var EndDate= document.admin.end_date.value;
	  var eDate = new Date(EndDate);
	  var sDate = new Date(StartDate);
	  if(StartDate!= "" && StartDate!= "" && sDate> eDate)
	    {
	    alert("The start date is earlier than the end Date!\n Please Make Sure that the end date is equal or greater than the start date.");
		  
		 document.admin.start_date.value="";
	    document.admin.end_date.value="";
		return false;
	    }
	}</script>
   <title>Admin | Online University Voting System</title>
</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li class='active'><a href='Admin_.jsp'><span>Manage Election</span></a></li>
   <li ><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul></div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>
<%ArrayList<String> post=(ArrayList<String>) request.getSession().getAttribute("Post"); 
ArrayList<String> dept=(ArrayList<String>) request.getSession().getAttribute("Department");
String nextId=(String)request.getSession().getAttribute("Id");
if(request.getSession().getAttribute("Admin")==null){
response.sendRedirect("index.jsp");
}else{
%>
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
<div id='vermenu'>
<ul>
   <li ><a href='UpdateElection.jsp'><span>Update Existing</span></a></li>
   <li class='active'><a href='Admin_.jsp'><span>Create New</span></a></li>
   <li class='last'><a href='DeleteElection.jsp'><span>Delete Existing</span></a></li>
   
</ul>
</div>
		</div>
		<div class="col-md-10">
		<center><ul id="design">
    <li><a  class="current"><b>Election Details</b></a></li>
    <li><a ><b>Add Candidates</b></a></li>
    <li><a ><b>Create Election</b></a></li>
    
    <li><a  ><b>Post Election</b></a></li>
</ul></center><br>

<form name="admin" action="ElectionManager" method="Post"  >
<input type="hidden" value="CreateElection_2.jsp" name="nextpage"/>
<input type="hidden" value="Admin_.jsp" name="requestFrom"/>
<table class="tg">
  <tr>
    <td class="tg-yw4l">Election ID:</td>
    <td class="tg-yw4l"><input type="text" name="election_id" readonly="readonly" value="<%=nextId%>"/></td>
    </tr>
  <tr>
    <td class="tg-yw4l">Post:</td>
    <td class="tg-yw4l"><select name="post" required="required"> 
    		<%for(int i=0;i<post.size();i++){ %>		<option value="<%=post.get(i)%>"><%=post.get(i)%></option>
    <%} %></select></td>
    <td class="tg-yw4l">of</td>
    <td class="tg-yw4l"><select name="department" required="required">
    <%for(int i=0;i<dept.size();i++){ %>		<option value="<%=dept.get(i)%>"><%=dept.get(i)%></option>
    <%} %>
    </select></td>
    <td class="tg-yw4l">for the year</td>
    <td class="tg-yw4l"><input type="text" name="year" required="required"/></td>
  </tr>
  <tr>
    <td class="tg-yw4l">Start Date:</td>
    <td class="tg-yw4l"><input type="date" name="start_date" required="required"/></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">End Date:</td>
    <td class="tg-yw4l"><input type="date" name="end_date" onblur="DateCheck()"/></td>
    
  </tr>
  <tr>
    <td class="tg-yw4l">Register Voters:</td>
    <td class="tg-yw4l"><input type="checkbox" onClick="toggle(this)" />Year</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><input type="checkbox" onClick="toggle2(this)" />Department</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><input type="checkbox" name="voter_year" value="1" >1st Year<br/>
<input type="checkbox" name="voter_year" value="2"> 2nd Year<br/>
<input type="checkbox" name="voter_year" value="3">3rd year<br/>
<input type="checkbox" name="voter_year" value="4">4th year<br/>
</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><input type="checkbox" name="voter_department" value="MBA">MBA<br/>
<input type="checkbox" name="voter_department" value="ECE"> ECE<br/>
<input type="checkbox" name="voter_department" value="CSE">CSE<br/>
<input type="checkbox" name="voter_department" value="IT">IT<br/>

    </td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>

</table>
    <center><input type="submit" value="Proceed" ></center></form>

<script language="JavaScript">
function toggle(source) {
	  checkboxes = document.getElementsByName('voter_year');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function toggle2(source) {
	  checkboxes = document.getElementsByName('voter_department');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}

	</script>
	

</div>
		
</div>
</div>
<%} %>
    </body>
</html>







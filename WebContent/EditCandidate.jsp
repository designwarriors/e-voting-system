<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList,candidate.*,user.user"%>


<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>Admin | Online University Voting System</title>
</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li ><a href='Admin_.jsp'><span>Manage Election</span></a></li>
   <li class='active'><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul>
</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>




    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
<div id='vermenu'>
<ul>
   <li class='active'><a href='EditCandidate.jsp'><span>Edit Candidate</span></a></li>
   <li ><a href='AddCandidate.jsp'><span>Add Candidate</span></a></li>
   <li class='last'><a href='RemoveCandidate.jsp'><span>Remove Candidate</span></a></li>
   
</ul>
</div>
		</div>
		<div class="col-md-10">
		
		
<% String id=request.getParameter("id");
ArrayList<Integer> Election_id=(ArrayList<Integer>)request.getSession().getAttribute("OngoingElectionId");
%>
<form action="ElectionManager" method="post">
<input type="hidden" value="EditCandidate.jsp" name="requestFrom"/>
<table class="tg">
  <tr>
    <td class="tg-yw4l">Election ID:</td>
    
    <td class="tg-yw4l"><select name="election_id">
    <%for(int i=0;i<Election_id.size();i++){ %>
    <option value="<%=Election_id.get(i)%>"><%=Election_id.get(i)%></option>
    <%} %></select></td>
    <td class="tg-yw4l"><input type="submit" value="Edit Candidate"/></td>
    </tr>
    
  
  </table>
  </form>
  <% if(id!=null){
	  ArrayList<Candidate> candidate=(ArrayList<Candidate>)request.getSession().getAttribute("Candidate");
	  
	  %>
  
<form action="ElectionManager" method="post">
<input type="hidden" value="EditCandidate.jsp" name="requestFrom"/>
<input type="hidden" value="EditCandidate" name="action"/>
<table class="tg">
  
  <tr>
    <td class="tg-yw4l"><p>Candidate ID:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p>Candidate Name:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p>Manifesto</td>
    <td class="tg-yw4l"></td>
 
     </tr>
    <%for(int i=0;i<candidate.size();i++){
	Candidate cand=new Candidate();
    cand=candidate.get(i);
    user User=new user();
    User.setUserId(cand.getCandidateId()); %>
  <tr>
    <td class="tg-yw4l"><%=cand.getCandidateId()%></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><%=User.getName() %></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><input type="text" placeholder="<%=cand.getManifesto() %>" id="manifesto"/></td>
  </tr>
<%} %>
<td class="tg-yw4l"><center><input type="submit" value="Proceed"></center></td>
    
</table>
    </form>
<%} %></div>
		
</div>
</div>

    </body>
</html>







<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="user.user"%>
<html lang=''>
<head> 
    <%@include file="stylesheet.jsp" %>

   <title>Online University Voting System</title>

</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li><a href='CastVote.jsp'><span>Cast Vote</span></a></li>
   <li><a href='Result_Voters.jsp'><span>View Results</span></a></li>
   <li><a href='UserAccount.jsp'><span>My Account</span></a></li>
   <li class='last active'><a href="UserAccount.jsp"><span>Logout</span></a></li>
</ul>
</div>
</center>

<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Voter</h4></div>
<br>

<%user User=(user)request.getSession().getAttribute("user"); %>
 <div class="container-fluid">
	<div class="row">	
		<div class="col-md-4"></div>
		<div class="col-md-8">
		<div class="col-md-2"></div>
		<div class="col-md-4"></div>
		<div class="col-md-2">
		<form action="AccountManager" method="post">
		<input type="hidden" value="UserAccount.jsp" name="requestFrom"/>
		<center><table  class="tg">
  <tr>
    <td class="tg-yw4l"><p>Name:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=User.getName() %></td>
    </tr>
    <tr>
    <td class="tg-yw4l"><p>Department:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=User.getDepartment() %></td>
    </tr>
    <tr>
    <td class="tg-yw4l"><p>Year:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=User.getYear() %></td>
    </tr>
    <tr>
    <td class="tg-yw4l"><p>Email:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=User.getUserEmail()%></td>
    </tr>
    <tr>
    <td class="tg-yw4l"><p>Password:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><input type="password" required="required"name="password"/></td>
    </tr>
    <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><input type="submit" value="Change Password" ></td>
    
    </tr>
    
    
    </table></center>
		
		</form>
		
	
		</div>
	
		</div>
		</div>
	</body>
</html>
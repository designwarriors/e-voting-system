<%@page import="election.ElectionDAO, user.user"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList,report.*,election.Election,candidate.*,user.*"%>

<html lang=''>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>Online University Voting System</title>

</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li ><a href='index.jsp'><span>Home</span></a></li>
   <li class='active'><a href='Election.jsp'><span>Ongoing Elections</span></a></li>
   <li ><a href='Result.jsp'><span>Results</span></a></li>
   <li class='last'><a href='Contact.jsp'><span>Contact</span></a></li>
</ul>
</div>
</center>
<br>



    <link href="css/bootstrap.min.css" rel="stylesheet">
    <%ArrayList<Election> election=new ElectionDAO().getElection(); %>
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
		</div>
		<div class="col-md-8">
		<%if(!election.isEmpty()){ %>
		<div class="panel-group" id="panel-331840">
		<%for(int i=election.size()-1;i>=0;i--){ 
			Election elect=new Election();
			elect=election.get(i);
			ArrayList<Integer> cand=new CandidateDAO().getCandidateIdByElectionId(elect.getElectionId());
		%>
		
		<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title" data-toggle="collapse" data-parent="#panel-331840" href="#panel-element-<%=elect.getElectionId()%>" style="width: auto;" ><%=elect.getPost()+" of "+elect.getDepartment()+ " for the year " +elect.getYear()%></a>
					</div>
					<div id="panel-element-<%=elect.getElectionId()%>" class="panel-collapse collapse">
						<div class="panel-body">
						<%for(int j=0;j<cand.size();j++){
						user User=new user();
						User.setUserId(cand.get(j));
						%>
						<p>	 <%=User.getName() %>
						
						<%} %><br>
						</div>
					</div>
				</div><br>
<%} %>
<%}else{ %>
<div class="col-md-2"></div>
<div class="col-md-5">
<hr><p>Sorry,There are no Elections to display. Thank You!</p><hr>
</div>
<%} %>
</div>

</div>
</div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>

<%@page import="election.ElectionDAO, user.user"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList,report.*,election.Election"%>

<html lang=''>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>Online University Voting System</title>

</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li ><a href='CastVote.jsp'><span>Cast Vote</span></a></li>
   <li class='active'><a href='Result_Voters.jsp'><span>View Results</span></a></li>
   <li><a href='UserAccount.jsp'><span>My Account</span></a></li>
   <li class='last'><a href="UserAccount.jsp"><span>Logout</span></a></li>
</ul>

</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Voter</h4></div>
<br>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <%ArrayList<Report> report=new ReportDAO().getAllResult(); 
    ArrayList<Election> election=new ElectionDAO().getElection(); %>
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
		</div>
		<div class="col-md-8">
		<br><div class="panel-group" id="panel-331840">
		<%for(int i=report.size()-1;i>=0;i--){ 
			Report report_obj=new Report();
		report_obj=report.get(i);
		Election elect=new Election();
		elect=new ElectionDAO().getElectionDetailsById(String.valueOf(report_obj.getElectionID()));
		user User=new user();
		User.setUserId(report_obj.getWinningCandidateId());
		%>
		
		<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title" data-toggle="collapse" data-parent="#panel-331840" href="#panel-element-<%=report_obj.getElectionID()%>" style="width: auto;" ><%=elect.getPost()+" of "+elect.getDepartment()+ " for the year " +elect.getYear()%></a>
					</div>
					<div id="panel-element-<%=report_obj.getElectionID()%>" class="panel-collapse collapse">
						<div class="panel-body">
						<p>	Total Votes: <%=report_obj.getTotalVotes()%><br>
						<p>	Winning Votes: <%=report_obj.getWinnerVotes()%><br>
						<p>	<%=User.name%> is the new <%=elect.getPost() %> of <%=elect.getDepartment() %> . <br>
						</div>
					</div>
				</div><br>
<%} %>
</div>
</div>
</div>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList,election.Election,user.user"%>


<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>Admin | Online University Voting System</title>
</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
  <li class='active'><a href='Admin_jsp'><span>Manage Election</span></a></li>
   <li><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul>
</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>




    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
<div id='vermenu'>
<ul>
   <li ><a href='UpdateElection.jsp'><span>Update Existing</span></a></li>
   <li class='active'><a href='Admin_.jsp'><span>Create New</span></a></li>
   <li class='last'><a href='DeleteElection.jsp'><span>Delete Existing</span></a></li>
   
</ul>
</div>
		</div>
		<div class="col-md-10">
		<center><ul id="design">
    <li><a  ><b>Election Details</b></a></li>
    <li><a ><b>Add Candidates</b></a></li>
    <li><a class="current" ><b>Create Election</b></a></li>
    
    <li><a  ><b>Post Election</b></a></li>
</ul></center><br>
<% Election election=(Election)request.getSession().getAttribute("ElectionDetails");
	
	if(election!=null){
		ArrayList<Integer> CandidateId=election.getCandidateId();
		ArrayList<String> Manifesto=election.getManifesto();
		
		if(Manifesto.isEmpty()){
			response.sendRedirect("CreateElection_2.jsp");
		}
%>
<h3>Election Summary</h3>
<hr></hr>
<form action="ElectionManager" method="post">
<table class="tg">
<input type="hidden" value="CreateElection_3.jsp" name="requestFrom"/>
  <tr>
    <td class="tg-yw4l">Election ID:</td>
    <td class="tg-yw4l"><input type="text" name="election_id" value="<%=election.getElectionId()%>" readonly="readonly"/></td>
    </tr>
  <tr>
    <td class="tg-yw4l">Post: </td>
    <td class="tg-yw4l"><input type="text" name="post" readonly="readonly"value="<%=election.getPost()%>"></td>
    <td class="tg-yw4l">of</td>
    <td class="tg-yw4l"><input type="text"  name="department" readonly="readonly" value="<%=election.getDepartment()%>"></td>
    <td class="tg-yw4l">for the year</td>
    <td class="tg-yw4l"><input type="text" name="year" readonly="readonly" value="<%=election.getYear()%>"/></td>
  </tr>
  <tr>
    <td class="tg-yw4l">Start Date:</td>
    <td class="tg-yw4l"><input type="datetime" name="start_date" readonly="readonly" value="<%=election.getStartDate() %>"/></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l">End Date:</td>
    <td class="tg-yw4l"><input type="datetime" name="end_date"readonly="readonly" value="<%=election.getEndDate() %>"/></td>
  </tr>
  <tr>
    <td class="tg-yw4l"> Voters:</td>
    <td class="tg-yw4l"><p>Year</p></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p>Department</p></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><%=election.getVoterYear() %></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><%=election.getVoterDepartment() %></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
  <tr>
    <td class="tg-yw4l"><p>ID:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p> Name:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p>Manifesto:</td>
     </tr>
  <tr>
    
    
  
  <%for(int i=0;i<CandidateId.size();i++){
	  user User=new user();
	  User.setUserId(CandidateId.get(i));%>
  <tr>
    <td class="tg-yw4l"><p><%=CandidateId.get(i) %></p></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=User.getName() %></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=Manifesto.get(i) %></p></td>
  </tr>
    <%} %>
  
<tr>
<td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><center><input type="submit" value="Proceed"></center></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>

</table>
    </form>

<script language="JavaScript">
function toggle(source) {
	  checkboxes = document.getElementsByName('voter_year');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}
function toggle2(source) {
	  checkboxes = document.getElementsByName('voter_department');
	  for(var i=0, n=checkboxes.length;i<n;i++) {
	    checkboxes[i].checked = source.checked;
	  }
	}</script>


</div>
		
</div>
</div>
<%}else{
response.sendRedirect("Admin_.jsp");
}
	%>

    </body>
</html>







<%@page import="java.beans.Customizer"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*,election.Election,user.user"%>

<html lang=''>
<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
      
   <title>Online University Voting System</title>

</head>
<body>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li class='active'><a href='CastVote.jsp'><span>Cast Vote</span></a></li>
   <li><a href='Result_Voters.jsp'><span>View Results</span></a></li>
   <li><a href='UserAccount.jsp'><span>My Account</span></a></li>
   <li class='last'><a href="UserAccount.jsp"><span>Logout</span></a></li>
</ul>

</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Voter</h4></div>
<br>

<%
if(request.getSession().getAttribute("user")==null)
response.sendRedirect("index.jsp");
else{
String selected="";
selected=(request.getParameter("id"));
String Userid=request.getParameter("cid");
user Voter=new user();
Voter.setUserId(Integer.parseInt(Userid));
ArrayList<Election> CustomisedElection=(ArrayList<Election>)request.getSession().getAttribute("CustomizedElection");
int size=CustomisedElection.size();
%>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <div class="container-fluid">
	<div class="row">
	
		<div class="col-md-3">
<div id='vermenu'>
<ul>
<%if(size==0){
%>
<li class="last active"><a href=''><span>Nothing </span></a></li>
<%} else{
	for(int i=0;i<CustomisedElection.size();i++){ 
	Election election=new Election();
	election=CustomisedElection.get(i); 
	  if(i!=(size-1)) {
	  if (election.getElectionId()==Integer.parseInt(selected)){
%>
	<li class="active"><a href='Voting_.jsp?id=<%=election.getElectionId()%>&cid=<%=Userid%>'><span><%=election.getPost()%></span></a></li>
<%} else{ %> 
<li ><a href='Voting_.jsp?id=<%=election.getElectionId()%>&cid=<%=Userid%>'><span><%=election.getPost()%></span></a></li>
	   
<%}}else{
if (election.getElectionId()==Integer.parseInt(selected)){%>
	 <li class="last active"><a href='Voting_.jsp?id=<%=election.getElectionId()%>&cid=<%=Userid%>'><span><%=election.getPost()%></span></a></li>
	   <%}else{ %>
	   <li class='last '><a href='Voting_.jsp?id=<%=election.getElectionId()%>&cid=<%=Userid%>'><span><%=election.getPost()%></span></a></li> 
	 <% }}}}%>
			  
		   
   
</ul>
</div></div>
<div class="col-md-6">
<form action="VotingManager" method="post">
<input type="hidden" name="election_id" value="<%=selected %>" >
<input type="hidden" name="userid" value="<%=Userid %>" >
<input type="hidden" name="requestFrom" value="Voting.jsp">


		<%for(int i=0;i<CustomisedElection.size();i++){
			Election election=new Election();
			election=CustomisedElection.get(i);
			if(election.getElectionId()!=Integer.parseInt(selected))
			continue;
			else{%>
<hr><p>Who should be the <%=election.getPost() %> of <%=election.getDepartment() %> for the Year <%=election.getYear() %>?</p><hr>
<div class="panel-group" id="panel-331840">
<%
ArrayList <Integer> candidate_id=election.getCandidateId();
		 ArrayList <String> manifesto=election.getManifesto();
		 for(int j=0;j<candidate_id.size();j++){
		 user User=new user();
		 User.setUserId(candidate_id.get(j));%>
  <p><input type="radio"  name="cand" value="<%=candidate_id.get(j)%>" checked="checked">   <%=User.getName()%></p>
		<div class="panel panel-default">
					<div class="panel-heading">
						 <a class="panel-title" data-toggle="collapse" data-parent="#panel-331840" href="#panel-element-<%=candidate_id.get(j)%>" style="width: auto;" >+ Know This Candidate</a>
					</div>
					<div id="panel-element-<%=candidate_id.get(j)%>" class="panel-collapse collapse">
						<div class="panel-body">
							<%=manifesto.get(j) %>
						</div>
					</div>
				</div>  
  <%}}} %>
<br><center><input type="submit" name="submit" value="Cast Vote" /></center>
</form>

		</div>
<div class="col-md-3">

</div>
</div>
</div>
<%} %>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>







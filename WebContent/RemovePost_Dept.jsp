<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.sql.*, java.util.ArrayList"%>


<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>Admin | Online University Voting System</title>
</head>
<body>
<%if(request.getSession().getAttribute("Admin")==null){
response.sendRedirect("index.jsp");
}else{
	 %>
<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li ><a href='Admin_.jsp'><span>Manage Election</span></a></li>
   <li ><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li class='active'><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul>
</div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>
<jsp:useBean id="dept" scope="session" class="department.DepartmentDAO"/>
<jsp:useBean id="post" scope="session" class="post.PostDAO"/>
<%ArrayList<String> Dept=dept.getAllDepartment();
ArrayList<String> Post=post.getAllPost();
 %>



    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-2">
<div id='vermenu'>
<ul>
   <li ><a href='AddDept.jsp'><span>Add Department</span></a></li>
   <li ><a href='AddCandidate.jsp'><span>Add Post</span></a></li>
   <li class='last active'><a href='RemovePost_Dept.jsp'><span>Remove Post/Dept</span></a></li>
   
</ul>
</div>
		</div>

		<div class="col-md-4">
		<form action="ElectionManager" method="post">
<input type="hidden" name="requestFrom" value="RemovePost_Dept.jsp"/>
<input type="hidden" name="action" value="RemovePost"/>
<table class="tg">
  <tr><td><hr><p>Post</p><hr></td></tr>
    <%for(int i=0;i<Post.size();i++){ %>
    <tr>
    <td class="tg-yw4l"><input type="checkbox" name="post" value="<%=Post.get(i)%>"/> <%=Post.get(i)%></td>
    </tr>
    
  <%} %>
  <tr>
        
</table>
<br><center><input type="submit" value="Delete"/></center>
</form>
</div>
		<div class="col-md-6">
<form action="ElectionManager" method="post">
<input type="hidden" name="requestFrom" value="RemovePost_Dept.jsp"/>
<input type="hidden" name="action" value="RemoveDept"/>

<table class="tg">
  <tr><td><hr><p>Department</p><hr></td></tr>
    <%for(int i=0;i<Dept.size();i++){ %>
    <tr>
    <td class="tg-yw4l"><input type="checkbox" name="dept" value="<%=Dept.get(i)%>"/> <%=Dept.get(i)%></td>
    </tr>
  <%} %>
  <tr>
        
</table>


<br><center><input type="submit" value="Delete"/></center>
    </form>
</div>

		
</div>
</div>
<%} %>
    </body>
</html>







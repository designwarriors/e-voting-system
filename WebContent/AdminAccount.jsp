<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.ArrayList,user.admin"%>


<head>
   <meta charset='utf-8'>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="css/styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
   <title>Admin | Online University Voting System</title>
</head>
<body>
   
<%if(request.getSession().getAttribute("Admin")==null){
response.sendRedirect("index.jsp");
}else{%>
	<center><h1>Online University Voting System</h1></center><br>
<center><div id='hormenu'>
<ul>
   <li class='active'><a href='Admin_.jsp'><span>Manage Election</span></a></li>
   <li ><a href='AddCandidate.jsp'><span>Manage Candidate</span></a></li>
   <li><a href='AddPost.jsp'><span>Manage Post</span></a></li>
   <li><a href='Report.jsp'><span> Report</span></a></li>
   <li class='last'><a href='AdminAccount.jsp'><span>My Account</span></a></li>
</ul></div>
</center>
<div align="right"><a href="Logout"><img alt="Logout" class="pic-circle-corner" src="images/user-icon.png" /></a><h4 >Welcome Admin</h4></div>
<br>

<%admin Admin=(admin)request.getSession().getAttribute("Admin"); %>
 <div class="container-fluid">
	<div class="row">	
		<div class="col-md-4"></div>
		<div class="col-md-8">
		<form action="AccountManager" method="post">
		<input type="hidden" value="AdminAccount.jsp" name="requestFrom"/>
		<center><table  class="tg">
  <tr>
    <td class="tg-yw4l"><p>Name:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=Admin.getName() %></td>
    </tr>
    <tr>
    <td class="tg-yw4l"><p>Email:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><%=Admin.getEmail()%></td>
    </tr>
    <tr>
    <td class="tg-yw4l"><p>Password:</td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"><p><input type="password" required="required"name="password"/></td>
    </tr>
    <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
    </tr>
    
    
    </table></center>
		<center><input type="submit" value="Change Password"></center>
		</form>
		
	
		</div>
	</div>
		</div>
		<%} %>
	</body>
</html>
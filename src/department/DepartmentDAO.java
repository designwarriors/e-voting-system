package department;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.DB_Conn;

public class DepartmentDAO {

public int getImmediateDeptID() {
	int id=0;
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlgetDeptId = "SELECT `Department_ID` from `department` order by `Department_ID` desc limit 1";

        PreparedStatement st = c.prepareStatement(sqlgetDeptId);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
        	id=rs.getInt("Department_ID")+1;
        }
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
return id;
}
public ArrayList<String> getAllDepartment()
{
	ArrayList<String> Dept=new ArrayList<String>();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlgetDept = "SELECT `Department` from `department`";

        PreparedStatement st = c.prepareStatement(sqlgetDept);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
        	Dept.add(rs.getString("Department"));
        }
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
	return Dept;
}

public boolean addDepartment(String deptDesc){
	boolean success=false;
	try {
		DB_Conn con = new DB_Conn();
	            Connection c = con.getConnection();
	           
	            String sqlAddDept = "INSERT into `department` values(NULL,'"+deptDesc+"') ";
	           
	            PreparedStatement st = c.prepareStatement(sqlAddDept);

	            int i=st.executeUpdate();
	           if(i>0)
	        	success=true;   
	}catch(SQLException sq){
		success=false;
	System.out.println("un");
}
	catch(Exception e){
		success=false;
		System.out.println("gun");
	}
	return success;
}

public boolean deleteDepartment(String department[]){
	
	int counter=0;
	if(department!=null){
	try {
		DB_Conn con = new DB_Conn();
	            Connection c = con.getConnection();
	           for(int i=0;i<department.length;i++){
	            String sqlDeleteDept = "DELETE from `department` where `Department`='"+department[i]+"' ";
	           
	            PreparedStatement st = c.prepareStatement(sqlDeleteDept);

	            int flag=st.executeUpdate();
	           if(flag>0)
	        	   counter++;
	        	   
	           }
	}catch(SQLException sq){
		
	System.out.println("un");
}
	catch(Exception e){
		
		System.out.println("gun");
	}}
	if (counter==department.length)
	return true;
	else
		return false;
}


}

package candidate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.DB_Conn;


public class CandidateDAO {
	ArrayList<Integer> Candidate_ID=new ArrayList<Integer>();
	ArrayList<String> manifesto=new ArrayList<String>();
	public ArrayList<Candidate> getCandidateDetailsByElectionId(int election_id)
	{
		ArrayList<Candidate> CandidateDetails=new ArrayList<Candidate>();
		try {
        	DB_Conn con = new DB_Conn();
            Connection c = con.getConnection();
            
            String sqlfetchCandidate = "SELECT `Candidate_ID`,`Manifesto` from `candidate` where `Election_ID`='"+election_id+"'";

            PreparedStatement st = c.prepareStatement(sqlfetchCandidate);
            ResultSet rs = st.executeQuery();
            while(rs.next())
            {
            Candidate candidate=new Candidate();
            candidate.setCandidateId(rs.getInt("Candidate_ID"));
            candidate.setManifesto(rs.getString("Manifesto"));
            CandidateDetails.add(candidate);
            }
	}catch(SQLException sq){
		sq.printStackTrace();
	}
	catch(Exception e){
		e.printStackTrace();
	}
		return CandidateDetails;
	}
	
	public ArrayList<Integer> getCandidateIdByElectionId(int election_id)
	{
		ArrayList<Integer> CandidateID=new ArrayList<Integer>();
		try {
        	DB_Conn con = new DB_Conn();
            Connection c = con.getConnection();
            
            String sqlfetchCandidate = "SELECT `Candidate_ID` from `candidate` where `Election_ID`='"+election_id+"'";

            PreparedStatement st = c.prepareStatement(sqlfetchCandidate);
            ResultSet rs = st.executeQuery();
            while(rs.next())
            {
            CandidateID.add(rs.getInt("Candidate_ID"));
            }
	}catch(SQLException sq){
		sq.printStackTrace();
	}
	catch(Exception e){
		e.printStackTrace();
	}
		return CandidateID;
	}

	
	public boolean addCandidate(int candidate_id, int election_id,String manifesto){
		boolean success=false;
		try {
        	DB_Conn con = new DB_Conn();
            Connection c = con.getConnection();
            
            String sqlAddCandidate = "INSERT into `candidate` values('"+election_id+"','"+candidate_id+"','"+manifesto+"') ";

            PreparedStatement st = c.prepareStatement(sqlAddCandidate);
            int i=st.executeUpdate();
            
            if(i>0)
            	success=true;
	}catch(SQLException sq){
		sq.printStackTrace();
	}
	catch(Exception e){
		e.printStackTrace();
	}

		return success;
	}
	public boolean RemoveCandidate( int election_id,String candidate_id[]){
		System.out.println(election_id);
		int counter=0;
		try {
        	DB_Conn con = new DB_Conn();
            Connection c = con.getConnection();
            for(int i=0;i<candidate_id.length;i++){
            	System.out.println(candidate_id[i]);
            	String sqlDeleteCandidate = "DELETE from `candidate` where `Election_ID`='"+election_id+"' AND `Candidate_ID`='"+candidate_id[i]+"'";

            PreparedStatement st = c.prepareStatement(sqlDeleteCandidate);
            int j=st.executeUpdate();
            
            if(j>0)
            	counter++;
            }
            
	}catch(SQLException sq){
		sq.printStackTrace();
	}
	catch(Exception e){
		e.printStackTrace();
	}
		if (counter==candidate_id.length)
		return true;
		else
			return false;
	}
	
	public static void main(String[]args){
		CandidateDAO dao=new CandidateDAO();
		ArrayList<Candidate> cand=dao.getCandidateDetailsByElectionId(2);
		System.out.println(cand.size());
	}
}

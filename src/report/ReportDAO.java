package report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import candidate.Candidate;
import candidate.CandidateDAO;
import database.DB_Conn;

public class ReportDAO {

public ArrayList<Integer> getResultByElectionId(int election_id,ArrayList<Integer>Candidates){
	ArrayList<Integer> result=new ArrayList<Integer>();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        for(int i=0;i<Candidates.size();i++){
        
        	String sqlUpdateProfile = "Select Count(`Election_ID`)as total from `votes` where `Candidate_ID`='"+Candidates.get(i)+"' AND `Election_ID`='"+election_id+"'";	
        	PreparedStatement st = c.prepareStatement(sqlUpdateProfile);
            ResultSet rs=st.executeQuery(sqlUpdateProfile);
            rs.next();
            
            result.add(rs.getInt(1));
        	}
        
        	
        
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}

	
	return result;
}
public boolean postResult(int election_id,ArrayList<Integer>Candidates,ArrayList<Integer> result){
	boolean success=false;
	int total=0, winner_id=0,winning_votes=0;
	for(int i=0;i<result.size();i++){
		total+=result.get(i);
		if(result.get(i)>winning_votes){
			winning_votes=result.get(i);
			winner_id=Candidates.get(i);
		}
	}
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        c.setAutoCommit(false);
        	String sqlUpdateProfile = "Insert into `result` values('"+election_id+"','"+total+"','"+winning_votes+"','"+winner_id+"')";	
        	PreparedStatement st = c.prepareStatement(sqlUpdateProfile);
            int i=st.executeUpdate();
            if(i>0)
            {  
            	String sqlUpdateElection = "UPDATE `election` set `status`='Complete' where `Election_ID`='"+election_id+"'";
            	PreparedStatement sta = c.prepareStatement(sqlUpdateElection);
                int j=sta.executeUpdate();
                if(j>0){
                success=true;
                c.commit();
                }
            }
        
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
	
	return success;
}
public ArrayList<Report> getAllResult(){
	ArrayList<Report> result=new ArrayList<Report>();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        	String sqlgetResult = "Select * from `result` ";	
        	PreparedStatement st = c.prepareStatement(sqlgetResult);
            ResultSet rs=st.executeQuery();
            while(rs.next()){
            Report report=new Report();
            report.election_id=rs.getInt("Election_ID");
            report.winning_votes=rs.getInt("Winning_Votes");
            report.winning_candidate_id=rs.getInt("Winning_Candidate_ID");
        	report.total_votes=rs.getInt("Total_Votes");
        	result.add(report);
            }
        
        	
        
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
	return result;
}
public ArrayList<Integer> getResultByID(int election_id){
	ArrayList<Integer> result=new ArrayList<Integer>();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        	String sqlgetResult = "Select * from `result` where `Election_ID`='"+election_id+"' ";	
        	PreparedStatement st = c.prepareStatement(sqlgetResult);
            ResultSet rs=st.executeQuery();
            while(rs.next()){
            
            result.add(rs.getInt("Election_ID"));
            result.add(rs.getInt("Winning_Votes"));
            result.add(rs.getInt("Winning_Candidate_ID"));
        	result.add(rs.getInt("Total_Votes"));
        	
            }
        
        	
        
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}

	
	return result;
}

public static void main(String []args){
	ReportDAO dao=new ReportDAO();
	ArrayList<Integer> input=new ArrayList <Integer>();
	input.add(1201);
	input.add(1202);
	input.add(1203);
	ArrayList<Integer> result=new ArrayList <Integer>();
	//result=dao.getResultByElectionId(2,input);
	for(int i=0;i<result.size();i++)
		System.out.println(result.get(i));
}
}

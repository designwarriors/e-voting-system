package user;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import election.Election;
import election.ElectionDAO;
import report.*;

/**
 * Servlet implementation class UserManager
 */
@WebServlet("/UserManager")
public class UserManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	ReportDAO report_dao=new ReportDAO();
	ElectionDAO election_dao=new ElectionDAO();
	ArrayList<Report> report=report_dao.getAllResult();
	ArrayList<Election>election=election_dao.getElection();
	request.getSession().setAttribute("AllResult", report);
	
	request.getSession().setAttribute("AllElection", election);
	//request.getRequestDispatcher("index.jsp");
	//response.sendRedirect("/index.jsp");
	//RequestDispatcher requestDispatcher=request.getRequestDispatcher("/index.jsp");
    //requestDispatcher.forward(request,response);
	
	
	}

}

package user;

import database.DB_Conn;

import java.sql.*;
import java.util.Scanner;

public class admin {
    public String Email = null;
    public int adminId = 0;
    
    public String name;
    public String contact;
    Connection c;
    
    public  void setAdminId(int x){
        this.adminId=x;
        fetchAllValues(x);
    }
    public String getEmail (){
        return Email;
    }
    public int getAdminId (){
        return adminId;
    }
    public String getName (){
        return name;
    } public boolean fetchAllValues (int adminId) {
        // GETS ALL THE VALUES FROM THE TABLE user-deails
        
        String fetchSql;
        boolean fetched=false;
        try{
        fetchSql = "SELECT * FROM  `admin` WHERE  `Admin_ID` =? ;";
        c = new DB_Conn() .getConnection();
        
        PreparedStatement psmt = c.prepareStatement(fetchSql);
        psmt.setInt(1, adminId);
        
        ResultSet executeQuery = psmt.executeQuery();
        boolean next = executeQuery.next();
        if (next){
        	
            name = executeQuery.getString("Name"); 
            Email = executeQuery.getString("Email_ID");
            contact = executeQuery.getString("Contact");
            fetched = true;
        }else {
        	
        	name = null;
            Email=null;
            contact=null;
            fetched = false;
        }
        }catch(SQLException sq){}
        catch(Exception e){}
        return fetched;
    }
    
    public static void main (String args[]) throws SQLException, ClassNotFoundException{
        System.out.println("Ok then gimme an email to give u an ID");
        Scanner sc = new Scanner (System.in);
        int next = sc.nextInt();
        admin admin = new admin ();
        admin.fetchAllValues(next);
        System.out.println(admin.name);
           System.out.println(admin.getEmail());
    }
}

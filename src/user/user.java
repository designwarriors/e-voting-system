package user;

import database.DB_Conn;

import java.sql.*;
import java.util.Scanner;

public class user {
    public String userEmail = null;
    public int userId = 0;
    
    public String name;
    public String year;
    public String department;
    public String section;
    
    Connection c;
    
    public  void setUserId(int x){
        this.userId=x;
        fetchAllValues(x);
    }
    public String getUserEmail (){
        return userEmail;
    }
    public int getUserId (){
        return userId;
    }
    public String getName (){
        return name;
    }
    public String getYear (){
        return year;
    }
    public String getDepartment (){
        return department;
    }
    public String getSection (){
        return section;
    }
   
    
    
    public boolean fetchAllValues (int userId) {
        // GETS ALL THE VALUES FROM THE TABLE user-deails
        
        String fetchSql;
        boolean fetched=false;
        try{
        fetchSql = "SELECT * FROM  `voter` WHERE  `User_ID` =? ;";
        c = new DB_Conn() .getConnection();
        
        PreparedStatement psmt = c.prepareStatement(fetchSql);
        psmt.setInt(1, userId);
        
        ResultSet executeQuery = psmt.executeQuery();
        boolean next = executeQuery.next();
        if (next){
            name = executeQuery.getString("Name"); 
            year = executeQuery.getString("Year");
            department = executeQuery.getString("Department");
            userEmail = executeQuery.getString("Email_ID");
            
            fetched = true;
        }else {
            name = null;
            year = null;
            department = null;
            section = null;
            fetched = false;
        }
        }catch(SQLException sq){}
        catch(Exception e){}
        return fetched;
    }
    
    public static void main (String args[]) throws SQLException, ClassNotFoundException{
        System.out.println("Ok then gimme an email to give u an ID");
        Scanner sc = new Scanner (System.in);
        int next = sc.nextInt();
        user user = new user ();
        user.fetchAllValues(next);
        System.out.println(user.name);
        System.out.println(user.getYear());
        System.out.println(user.getDepartment());
    }
}

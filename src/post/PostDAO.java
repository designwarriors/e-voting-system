package post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.DB_Conn;

public class PostDAO {

public int getImmediatePostID() {
	int id=0;
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlgetPostId = "SELECT `Post_ID` from `post` order by `Post_ID` desc limit 1";

        PreparedStatement st = c.prepareStatement(sqlgetPostId);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
        	id=rs.getInt("Post_ID")+1;
        }
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
return id;
}
public ArrayList<String> getAllPost()
{
	ArrayList<String> Post=new ArrayList<String>();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlgetPost = "SELECT `Post` from `post`";

        PreparedStatement st = c.prepareStatement(sqlgetPost);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
        	Post.add(rs.getString("Post"));
        }
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
	return Post;
}
public boolean addPost(String postDesc){
	boolean success=false;
	try {
		DB_Conn con = new DB_Conn();
	            Connection c = con.getConnection();
	           
	            String sqlAddPost = "INSERT into post values(NULL,'"+postDesc+"') ";
	           
	            PreparedStatement st = c.prepareStatement(sqlAddPost);

	            int i=st.executeUpdate();
	           if(i>0)
	        	success=true;   
	}catch(SQLException sq){
		success=false;
	System.out.println("un");
}
	catch(Exception e){
		success=false;
		System.out.println("gun");
	}
	return success;
}
public boolean deletePost(String post[]){
	
	int counter=0;
	if(post.length!=0){
	try {
		DB_Conn con = new DB_Conn();
	            Connection c = con.getConnection();
	           for(int i=0;i<post.length;i++){
	            String sqlDeletePost = "DELETE from `post` where `Post`='"+post[i]+"' ";
	           
	            PreparedStatement st = c.prepareStatement(sqlDeletePost);

	            int flag=st.executeUpdate();
	           if(flag>0)
	        	   counter++;
	        	   
	           }
	}catch(SQLException sq){
		
	System.out.println("un");
}
	catch(Exception e){
		
		System.out.println("gun");
	}}
	if (post.length==counter)
	return true;
	else
		return false;
}

public static void main(String args[]){
	ArrayList<String>post=new ArrayList<String>();
	PostDAO dao=new PostDAO();
	post=dao.getAllPost();
	System.out.println(post.get(2));
}
}

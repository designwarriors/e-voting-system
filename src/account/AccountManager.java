package account;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import user.admin;
import user.user;

/**
 * Servlet implementation class AccountManager
 */
@WebServlet("/AccountManager")
public class AccountManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AccountManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		RequestDispatcher requestDispatcher;
		AccountDAO account_dao=new AccountDAO();
		user User=(user)request.getSession().getAttribute("user");
		admin Admin=(admin)request.getSession().getAttribute("Admin");
		String requestFrom=request.getParameter("requestFrom");
		String password=request.getParameter("password");
		if(requestFrom.equalsIgnoreCase("UserAccount.jsp")){
			boolean success=account_dao.updateUserProfile(User.getUserId(),password);
			if(success){
				out.println("<script>alert('Password Changed Successfuly!');</script>");
				requestDispatcher=getServletContext().getRequestDispatcher("/UserAccount.jsp");
		        requestDispatcher.include(request,response);
				
			}
			else{
				out.println("<script>alert('Request Unsuccessful!');</script>");
				requestDispatcher=getServletContext().getRequestDispatcher("/UserAccount.jsp");
		        requestDispatcher.include(request,response);
				}
		}
		else if(requestFrom.equalsIgnoreCase("AdminAccount.jsp")){
			
			boolean success=account_dao.updateAdminProfile(Admin.getAdminId(),password);
			if(success){
				
				out.println("<script>alert('Password Changed Successfuly!');</script>");
				requestDispatcher=getServletContext().getRequestDispatcher("/AdminAccount.jsp");
		        requestDispatcher.include(request,response);
				
			}
			else{
				out.println("<script>alert('Request Unsuccessful!');</script>");
		        requestDispatcher=getServletContext().getRequestDispatcher("/AdminAccount.jsp");
		        requestDispatcher.include(request,response);
			}
	}
	}
}

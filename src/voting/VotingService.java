package voting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import candidate.CandidateDAO;
import candidate.Candidate;
import user.user;
import election.Election;
import election.ElectionDAO;

public class VotingService {
	
	public ArrayList<Election> showEligibleElection(int user_id){
		VotingService votingService=new VotingService();
		ArrayList<Election>Election =new ArrayList<Election>();
		
		ArrayList<Election>CustomisedElection =new ArrayList<Election>();
		CustomisedElection.clear();
		user User=new user();
		User.setUserId(user_id);
		ElectionDAO election_dao=new ElectionDAO();
		VotingDAO voting_dao=new VotingDAO();
		Election=election_dao.getElection();
		ArrayList<Integer> voted=voting_dao.getVotedElections(user_id);
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		for(int i=0;i<Election.size();i++){
			boolean check=false;
			Election election=new Election();
			election=Election.get(i);
			if(voted.contains(election.getElectionId()))
				continue;
			ArrayList<String>voter_year=votingService.Slasher(election.getVoterYear());
			ArrayList<String>voter_department=votingService.Slasher(election.getVoterDepartment());
			
			for(int j=0;j<voter_year.size();j++){
				
				if(voter_year.get(j).equalsIgnoreCase(User.getYear()))
					{
					
					for(int k=0;k<voter_department.size();k++){
						
						if(voter_department.get(k).equalsIgnoreCase(User.getDepartment()))
							{
							check=true;
							
							break;
							}
					}
					break;
					}
			}
			if(check){
			 String start_date=election.getStartDate();
			 
			 String end_date=election.getEndDate();
			 
			 try{
				 Date today = formatter.parse(formatter.format(new Date()));
			 Date StartDate = formatter.parse(start_date);
			 Date EndDate = formatter.parse(end_date);
			 
			 int isStartDateReached=today.compareTo(StartDate);
			 int isEndDateReached=today.compareTo(EndDate);
			 
			 if(isStartDateReached>-1 && isEndDateReached <1){
				 CandidateDAO candidate_dao=new CandidateDAO();
				 ArrayList<Candidate>candidate_details=(ArrayList<Candidate>)candidate_dao.getCandidateDetailsByElectionId(election.getElectionId());
				 for(int c=0;c<candidate_details.size();c++){
					 Candidate cand=new Candidate();
					 cand=candidate_details.get(c);
					 if(cand.getElectionId()==election.getElectionId()){
					 election.setCandidateId(cand.getCandidateId());
					 election.setManifesto(cand.getManifesto());
					 }
				 }
				 CustomisedElection.add(election);
			 }
			 
			 //else
				// continue;
			 //boolean isBefore = formatter.format(date).before(date2);
			 //boolean isAfter  = date1.after (date2);
			 }catch(ParseException e){
				 e.printStackTrace();
				 }

			}
		}
		
		return CustomisedElection;
		}
	public ArrayList<String> Slasher(String input){
		ArrayList<String> output=new ArrayList<String>();
		StringTokenizer st=new StringTokenizer(input," ");
		while(st.hasMoreTokens()){
			output.add(st.nextToken());
		}
	return output;
	}
	
public static void main(String[]args){
	VotingService vs=new VotingService();
	ArrayList<Election> h=vs.showEligibleElection(123);
	
	for(int i=0;i<h.size();i++){
		Election election=new Election();
		election=h.get(i);
		ArrayList<Integer> u=election.getCandidateId();
		System.out.println(u.size());
	}
}
}

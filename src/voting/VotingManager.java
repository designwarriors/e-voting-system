package voting;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import post.PostDAO;
import user.user;
import department.DepartmentDAO;
import election.Election;
import election.ElectionDAO;
import election.ElectionService;

/**
 * Servlet implementation class VotingManager
 */
@WebServlet("/VotingManager")
public class VotingManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VotingManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		Election election=new Election();
		ElectionDAO election_dao=new ElectionDAO();
		VotingService vs=new VotingService();
		
		RequestDispatcher requestDispatcher;
		String referer=(String)request.getSession().getAttribute("Referer");
		String requestFrom=request.getParameter("requestFrom");
		if("Login".equalsIgnoreCase(referer)){
		ArrayList<Election> customizedElection=new ArrayList<Election>();
		user User=(user)request.getSession().getAttribute("user");
		System.out.println(User.getUserId());
		customizedElection=vs.showEligibleElection(User.getUserId());
		System.out.println(customizedElection.size());
		request.getSession().setAttribute("CustomizedElection", customizedElection);
		request.getSession().removeAttribute("Referer");
		request.getSession().setAttribute("user",User);
        referer.concat("0000");
        requestDispatcher=getServletContext().getRequestDispatcher("/CastVote.jsp");
        requestDispatcher.forward(request,response);
		}
		else if(requestFrom.equalsIgnoreCase("Voting.jsp")){
			String userid=request.getParameter("userid");
			String election_id=request.getParameter("election_id");
			String candidate_id=request.getParameter("cand");
			user User=(user)request.getSession().getAttribute("user");
			VotingDAO voting_dao=new VotingDAO();
			boolean success=voting_dao.CastVote(Integer.parseInt(candidate_id), Integer.parseInt(election_id), User.getUserId());
			if(success)
			{		
			ArrayList<Election> customizedElection=new ArrayList<Election>();
			
			
			customizedElection=vs.showEligibleElection( User.getUserId());
			System.out.println(customizedElection.size());
			request.getSession().setAttribute("CustomizedElection", customizedElection);
			request.getSession().setAttribute("user",User);
			out.println("<script>alert('Voted Successfuly!');</script>");
	        requestDispatcher=getServletContext().getRequestDispatcher("/CastVote.jsp");
	        requestDispatcher.include(request,response);
			
		}else{
				out.println("<script>alert('Vote UnSuccessful!');</script>");
			requestDispatcher=getServletContext().getRequestDispatcher("/CastVote.jsp");
	        requestDispatcher.include(request,response);
			
		}
	}
	}
}

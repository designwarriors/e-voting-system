package election;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import candidate.Candidate;
import candidate.CandidateDAO;
import database.DB_Conn;

public class ElectionDAO {

public ArrayList<Election> getElection()
 {
	ArrayList<Election> ElectionDetails=new ArrayList<Election>();
	try {
        	DB_Conn con = new DB_Conn();
            Connection c = con.getConnection();
            
            String sqlfetchElection = "SELECT * from `election`";

            PreparedStatement st = c.prepareStatement(sqlfetchElection);
            ResultSet rs = st.executeQuery();
            while(rs.next())
            {
            Election election=new Election();            
            election.setElectionId(rs.getInt("Election_ID"));
            election.setPost(rs.getString("Post"));
            election.setDepartment(rs.getString("Department"));
            election.setYear(rs.getString("Year"));
            election.setStartDate(rs.getString("Start_Date"));
            election.setEndDate(rs.getString("End_Date"));
            election.setVoterYear(rs.getString("Voter_Year"));
            election.setVoterDepartment(rs.getString("Voter_Department"));
            CandidateDAO candidate=new CandidateDAO();
            ArrayList<Candidate> candidateDeatils=candidate.getCandidateDetailsByElectionId(election.getElectionId());
            for(int i=0;i<candidateDeatils.size();i++){
            	Candidate retrievedCandidate=(Candidate)candidateDeatils.get(i);
            	election.setCandidateId(retrievedCandidate.getCandidateId());
            	election.setManifesto(retrievedCandidate.getManifesto());
            }
            
            ElectionDetails.add(election);
            }
	}catch(SQLException sq){
		sq.printStackTrace();
	}
	catch(Exception e){
		e.printStackTrace();
	}
	
	return ElectionDetails;
 }

public Election getElectionDetailsById(String election_id){
	Election election=new Election();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlfetchElection = "SELECT * from `election` where `Election_ID`='"+Integer.parseInt(election_id)+"'";

        PreparedStatement st = c.prepareStatement(sqlfetchElection);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
            
        election.setElectionId(rs.getInt("Election_ID"));
        election.setPost(rs.getString("Post"));
        election.setDepartment(rs.getString("Department"));
        election.setYear(rs.getString("Year"));
        election.setStartDate(rs.getString("Start_Date"));
        election.setEndDate(rs.getString("End_Date"));
        election.setVoterYear(rs.getString("Voter_Year"));
        election.setVoterDepartment(rs.getString("Voter_Department"));
        }
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}

	return election;
}
public boolean updateElection(String election_id,String post,String dept,String year,String start_date,String end_date,String voter_year,String voter_department)
{
boolean success=false;
try {
    DB_Conn con = new DB_Conn();
    
        Connection c = con.getConnection();
        
        String sqlUpdateElection = "Update `election` set `Post`='"+post+"',`Department`='"+dept+"',`Year`='"+year+"',`Start_Date`='"+start_date+"',`End_Date`='"+end_date+"',`Voter_Year`='"+voter_year+"',`Voter_Department`='"+voter_department+"' where `Election_ID`='"+Integer.parseInt(election_id)+"'";
        
        PreparedStatement st = c.prepareStatement(sqlUpdateElection);
       int i=st.executeUpdate();
       if(i>0){
       success=true;
       }
       else
    	   success=false;
       }catch(SQLException sq){
       	sq.printStackTrace();
       }
       catch(Exception e){
       	e.printStackTrace();
       }

return success;
}
public boolean deleteElection(String election_id)
{
boolean success=false;
try {
    DB_Conn con = new DB_Conn();
    
        Connection c = con.getConnection();
        c.setAutoCommit(false);
        String sqlDeleteElection = "Delete from `election` where `Election_ID`='"+Integer.parseInt(election_id)+"'";
        
        PreparedStatement st = c.prepareStatement(sqlDeleteElection);
       int i=st.executeUpdate();
       if(i>0){
       
       PreparedStatement ST = c.prepareStatement("DELETE from `candidate` where `Election_ID`='"+Integer.parseInt(election_id)+"' ");
       int j=ST.executeUpdate();
       if(j>0){
    	   success=true;
    	   c.commit();
       }
       else
    	   success=false;
       }
}catch(SQLException sq){
       	sq.printStackTrace();
       }
       catch(Exception e){
       	e.printStackTrace();
       }

return success;
}
public ArrayList<Integer> getAllElectionID() {

	ArrayList<Integer> id=new ArrayList<Integer>();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlgetElectionId = "SELECT `Election_ID` from `election`";

        PreparedStatement st = c.prepareStatement(sqlgetElectionId);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        	id.add(rs.getInt("Election_ID"));
        
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
return id;
}
public ArrayList<Integer> getAllCompletedElectionID() {

	ArrayList<Integer> id=new ArrayList<Integer>();
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlgetElectionId = "SELECT `Election_ID` from `election` where `status`='Complete'";

        PreparedStatement st = c.prepareStatement(sqlgetElectionId);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        	id.add(rs.getInt("Election_ID"));
        
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
return id;
}

public int getImmediateElectionID() {

	int id=0;
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlgetElectionId = "SELECT `Election_ID` from `election` order by `Election_ID` desc limit 1";

        PreparedStatement st = c.prepareStatement(sqlgetElectionId);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {
        	id=rs.getInt("Election_ID")+1;
        }
}catch(SQLException sq){
	sq.printStackTrace();
}
catch(Exception e){
	e.printStackTrace();
}
return id;
}
public boolean createElection(Election election){
	boolean success=false;
	int flag=0;
	try {
        DB_Conn con = new DB_Conn();
        
            Connection c = con.getConnection();
            c.setAutoCommit(false);
            String sqlCreateElection = "insert into `election` values('"+election.getElectionId()+"','"+election.getPost()+"','"+election.getDepartment()+"','"+election.getYear()+"','"+election.getStartDate()+"','"+election.getEndDate()+"','"+election.getVoterYear()+"','"+election.getVoterDepartment()+"','Pending')";
            
            PreparedStatement st = c.prepareStatement(sqlCreateElection);
           int i=st.executeUpdate();
           if(i>0){
        	success=true;   
        	   ArrayList<Integer>id=election.getCandidateId();
        	   ArrayList<String>manifesto=election.getManifesto();
        	   for(int index=0;index<id.size();index++){

                   PreparedStatement ST = c.prepareStatement("INSERT into `candidate` values('"+election.getElectionId()+"','"+id.get(index)+"','"+manifesto.get(index)+"')");
                  int j=ST.executeUpdate();
                  if(j>0)flag++;
                  else{
                	  success=false;
                	  break;
                  }
        	   }
           if(flag==id.size())
        	   c.commit();
           }
           else{
        	   success=false;
           }
           }catch(Exception ex){
		ex.printStackTrace();
	}

	return success;
}

public ArrayList<Integer> getOngoingElectionID(){
	ArrayList<Integer>election_id=new ArrayList<Integer>();
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	try {
    	DB_Conn con = new DB_Conn();
        Connection c = con.getConnection();
        
        String sqlfetchElection = "SELECT `Election_ID`,`End_Date` from `election`";

        PreparedStatement st = c.prepareStatement(sqlfetchElection);
        ResultSet rs = st.executeQuery();
        while(rs.next())
        {

			 try{
				 Date today = formatter.parse(formatter.format(new Date()));
			 
			 Date EndDate = formatter.parse(rs.getString("End_Date"));
			 
			 int isEndDateReached=today.compareTo(EndDate);
			 if(isEndDateReached<1){
				 election_id.add(rs.getInt("Election_ID"));
			 }
			 }catch(ParseException e){
				 e.printStackTrace();
				 }
        	
        }
	}catch(SQLException sq){
		sq.printStackTrace();
	}
	catch(Exception e){
		e.printStackTrace();
	}

	return election_id;
	
}
public static void main(String []args){
	ElectionDAO dao=new ElectionDAO();
	//ArrayList<Election> election=dao.getElection();
	//for(int i=0;i<election.size();i++){
	//Election el=new Election();	
	//el=election.get(i);
	//ArrayList<Integer>can=el.getCandidateId();
	//System.out.println(can.get(0));
	//}
	ArrayList<Integer>id=dao.getAllElectionID();
	System.out.println(id);
	ArrayList<Integer>eid=dao.getOngoingElectionID();
	System.out.println(eid);
}
}

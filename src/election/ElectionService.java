package election;

public class ElectionService {
	Election election=new Election();
	public Election setElectionDetails(String election_id,String post,String department,String year,String startDate,String endDate, String voter_year, String voter_department)
	{
		
		election.setElectionId(Integer.parseInt(election_id));
		election.setPost(post);
		election.setDepartment(department);
		election.setYear(year);
		election.setStartDate(startDate);
		election.setEndDate(endDate);
		election.setVoterYear(voter_year);
		election.setVoterDepartment(voter_department);
	return election;
	}
	public Election setCandidateDetails(Election elect,String Candidate_id,String manifesto)
	{
		elect.setCandidateId(Integer.parseInt(Candidate_id));
		elect.setManifesto(manifesto);
	return elect;
	}
}

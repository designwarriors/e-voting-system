package election;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import candidate.*;
import department.DepartmentDAO;
import post.PostDAO;
import report.ReportDAO;

/**
 * Servlet implementation class ElectionManager
 */
@WebServlet("/ElectionManager")
public class ElectionManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ElectionManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		ElectionService election_service=new ElectionService();
		Election election=new Election();
		ElectionDAO election_dao=new ElectionDAO();
		CandidateDAO candidate_dao=new CandidateDAO();
		PostDAO post_dao=new PostDAO();
		DepartmentDAO dept_dao=new DepartmentDAO();
		ReportDAO report_dao=new ReportDAO();
		RequestDispatcher requestDispatcher;
		String referer=(String)request.getSession().getAttribute("Referer");
		String requestFrom=request.getParameter("requestFrom");
		ArrayList<Integer> all_election_id=election_dao.getAllElectionID();
		request.getSession().setAttribute("ElectionId",all_election_id);
		ArrayList<Integer> ongoingElectionID=election_dao.getOngoingElectionID();
		request.getSession().setAttribute("OngoingElectionId",ongoingElectionID);
		//System.out.println(all_election_id);
		//System.out.println(ongoingElectionID);
		ArrayList<Integer> completedElectionID=election_dao.getAllCompletedElectionID();
		ArrayList<Integer> finishedElection=new ArrayList<Integer>();
		finishedElection = (ArrayList<Integer>)all_election_id.clone();
		finishedElection.removeAll(ongoingElectionID);
		finishedElection.removeAll(completedElectionID);
		//System.out.println(all_election_id);
		//System.out.println(completedElectionID);
		//System.out.println(ongoingElectionID);
		request.getSession().setAttribute("FinishedElectionId",finishedElection);
		
		if("Login".equalsIgnoreCase(referer)){
		
		ArrayList<String> post=post_dao.getAllPost();
		ArrayList<String> dept=dept_dao.getAllDepartment();
		
		String immediateEletionId=""+election_dao.getImmediateElectionID();
		request.getSession().setAttribute("Id", immediateEletionId);
		request.getSession().setAttribute("Post", post);
        request.getSession().setAttribute("Department", dept);
        request.getSession().removeAttribute("Referer");
        referer.concat("0000");
        requestDispatcher=getServletContext().getRequestDispatcher("/Admin_.jsp");
        requestDispatcher.forward(request,response);
		}
		else if(requestFrom.equalsIgnoreCase("Admin_.jsp")){
			String election_id=request.getParameter("election_id");
			String post=request.getParameter("post");
			String dept=request.getParameter("department");
			String year=request.getParameter("year");
			String start_date=request.getParameter("start_date");
			String end_date=request.getParameter("end_date");
			String voter_year[]=request.getParameterValues("voter_year");
			String voter_department[]=request.getParameterValues("voter_department");
			String page="/"+request.getParameter("nextpage");
			StringBuffer voter_year_prepend=new StringBuffer();
		    StringBuffer voter_department_prepend=new StringBuffer();
		    for(int i=0;i<voter_year.length;i++){
		    	voter_year_prepend.append(voter_year[i]+" ");
		    }
		    for(int i=0;i<voter_department.length;i++){
		    	voter_department_prepend.append(voter_department[i]+" ");
		    }
		
		election=election_service.setElectionDetails(election_id, post, dept, year, start_date, end_date, voter_year_prepend.toString(), voter_department_prepend.toString());
		request.getSession().setAttribute("ElectionDetails", election);
        requestDispatcher=getServletContext().getRequestDispatcher(page);
        requestDispatcher.forward(request,response);
		}
		else if(requestFrom.equalsIgnoreCase("CreateElection_2.jsp")){
			String candidate_id=request.getParameter("candidate_id");
			String manifesto=request.getParameter("manifesto");	
			
			election=election_service.setCandidateDetails((Election)request.getSession().getAttribute("ElectionDetails"),candidate_id, manifesto);
			request.getSession().setAttribute("ElectionDetails", election);
			requestDispatcher=getServletContext().getRequestDispatcher("/"+requestFrom);
	        requestDispatcher.forward(request,response);
			
  }
		else if(requestFrom.equalsIgnoreCase("ProceedToSummary")){
			election=(Election)request.getSession().getAttribute("ElectionDetails");
			request.getSession().setAttribute("ElectionDetails", election);
			requestDispatcher=getServletContext().getRequestDispatcher("/CreateElection_3.jsp");
	        requestDispatcher.forward(request,response);
			
  }
		else if(requestFrom.equalsIgnoreCase("CreateElection_3.jsp")){
			all_election_id=election_dao.getAllElectionID();
			request.getSession().setAttribute("ElectionId",all_election_id);
			ongoingElectionID=election_dao.getOngoingElectionID();
			request.getSession().setAttribute("OngoingElectionId",ongoingElectionID);
			//System.out.println(all_election_id);
			//System.out.println(ongoingElectionID);
			completedElectionID=election_dao.getAllCompletedElectionID();
			finishedElection=new ArrayList<Integer>();
			finishedElection = (ArrayList<Integer>)all_election_id.clone();
			finishedElection.removeAll(ongoingElectionID);
			finishedElection.removeAll(completedElectionID);
			//System.out.println(all_election_id);
			//System.out.println(completedElectionID);
			//System.out.println(ongoingElectionID);
			request.getSession().setAttribute("FinishedElectionId",finishedElection);
			
			election=(Election)request.getSession().getAttribute("ElectionDetails");
			request.getSession().setAttribute("ElectionDetails", election);
			boolean success=election_dao.createElection((Election)request.getSession().getAttribute("ElectionDetails"));
			String immediateEletionId=""+election_dao.getImmediateElectionID();
			request.getSession().setAttribute("Id", immediateEletionId);
			if(success){
				out.println("<script>alert('The election has been added!');</script>");
				requestDispatcher=request.getRequestDispatcher("/Admin_.jsp");
				requestDispatcher.include(request,response);
				}
			else{
				out.println("<script>alert('The election could not be added!');</script>");
				requestDispatcher=request.getRequestDispatcher("/Admin_.jsp");
				requestDispatcher.include(request,response);
			
			}
			
  }
		else if(requestFrom.equalsIgnoreCase("UpdateElection.jsp")&& "UpdateElection".equalsIgnoreCase(request.getParameter("action")))
		{
			String election_id=request.getParameter("election_id");
			String post=request.getParameter("post");
			String dept=request.getParameter("department");
			String year=request.getParameter("year");
			String start_date=request.getParameter("start_date");
			String end_date=request.getParameter("end_date");
			String voter_year[]=request.getParameterValues("voter_year");
			String voter_department[]=request.getParameterValues("voter_department");
			//String page="/"+request.getParameter("nextpage");
			StringBuffer voter_year_prepend=new StringBuffer();
		    StringBuffer voter_department_prepend=new StringBuffer();
		    for(int i=0;i<voter_year.length;i++){
		    	voter_year_prepend.append(voter_year[i]+" ");
		    }
		    for(int i=0;i<voter_department.length;i++){
		    	voter_department_prepend.append(voter_department[i]+" ");
		    }
		
			boolean success=election_dao.updateElection(election_id,post,dept,year,start_date,end_date,voter_year_prepend.toString(),voter_department_prepend.toString());
			if(success){
				out.println("<script>alert('The election has been updated!');</script>");
				requestDispatcher=getServletContext().getRequestDispatcher("/UpdateElection.jsp");
				requestDispatcher.include(request, response);
			}
			else{
				out.println("<script>alert('The election could not be updated! Try again Later!');</script>");
				requestDispatcher=getServletContext().getRequestDispatcher("/UpdateElection.jsp");
				requestDispatcher.include(request, response);
				
			}
		}

		else if(requestFrom.equalsIgnoreCase("UpdateElection.jsp")){
		Election electionById=(Election)election_dao.getElectionDetailsById(request.getParameter("election_id"));
		ArrayList<String> post=post_dao.getAllPost();
		ArrayList<String> dept=dept_dao.getAllDepartment();
		String immediateEletionId=""+election_dao.getImmediateElectionID();
		request.getSession().setAttribute("Id", immediateEletionId);
		request.getSession().setAttribute("Post", post);
        request.getSession().setAttribute("Department", dept);
        request.getSession().setAttribute("ElectionDetailsById", electionById);
		requestDispatcher=getServletContext().getRequestDispatcher("/UpdateElection.jsp?id="+request.getParameter("election_id"));
        requestDispatcher.include(request,response);
		}
		else if(requestFrom.equalsIgnoreCase("DeleteElection.jsp")&& "DeleteElection".equalsIgnoreCase(request.getParameter("action")))
		{
			boolean success=election_dao.deleteElection(request.getParameter("election_id"));
			if(success){
			out.println("<script>alert('The election has been deleted!');</script>");
			requestDispatcher=getServletContext().getRequestDispatcher("/DeleteElection.jsp");
			requestDispatcher.include(request,response);
			
		}
			else{
				out.println("<script>alert('The election could not be deleted!');</script>");
				requestDispatcher=getServletContext().getRequestDispatcher("/DeleteElection.jsp");
				requestDispatcher.include(request,response);
				
			
			}
		}
		else if(requestFrom.equalsIgnoreCase("DeleteElection.jsp")){
			Election electionById=(Election)election_dao.getElectionDetailsById(request.getParameter("election_id"));
			ArrayList<String> post=post_dao.getAllPost();
			ArrayList<String> dept=dept_dao.getAllDepartment();
			String immediateEletionId=""+election_dao.getImmediateElectionID();
			request.getSession().setAttribute("Id", immediateEletionId);
			request.getSession().setAttribute("Post", post);
	        request.getSession().setAttribute("Department", dept);
	        request.getSession().setAttribute("ElectionDetailsById", electionById);
			requestDispatcher=getServletContext().getRequestDispatcher("/DeleteElection.jsp?id="+request.getParameter("election_id"));
	        requestDispatcher.forward(request,response);
			}
		else if(requestFrom.equalsIgnoreCase("AddPost.jsp")){
		boolean success=post_dao.addPost(request.getParameter("post"));
		out.println("<script>alert('The post has been added!');</script>");
		requestDispatcher=request.getRequestDispatcher("AddPost.jsp");
		requestDispatcher.include(request,response);
		}
		else if(requestFrom.equalsIgnoreCase("AddDept.jsp")){
			boolean success=dept_dao.addDepartment(request.getParameter("dept"));
			out.println("<script>alert('The department has been added!');</script>");
			requestDispatcher=request.getRequestDispatcher("AddDept.jsp");
			requestDispatcher.include(request,response);
		}
		else if(requestFrom.equalsIgnoreCase("AddCandidate.jsp")){
		String candidate_id=request.getParameter("candidate_id");
		String election_id=request.getParameter("election_id");
		String manifesto=request.getParameter("manifesto");
			boolean success=new CandidateDAO().addCandidate(Integer.parseInt(candidate_id),Integer.parseInt( election_id), manifesto);
			if(success){
				out.println("<script>alert('The Candidate has been added!');</script>");
			requestDispatcher=request.getRequestDispatcher("/AddCandidate.jsp");
			requestDispatcher.include(request,response);
		}
			else{
				out.println("<script>alert('This candidate is not added!');</script>");
				requestDispatcher=request.getRequestDispatcher("/AddCandidate.jsp");
				requestDispatcher.include(request,response);
			}
		}
		
		else if(requestFrom.equalsIgnoreCase("EditCandidate.jsp")){
			ArrayList<Candidate> cand=candidate_dao.getCandidateDetailsByElectionId(Integer.parseInt(request.getParameter("election_id")));
			request.getSession().setAttribute("Candidate",cand);
			requestDispatcher=getServletContext().getRequestDispatcher("/EditCandidate.jsp?id="+request.getParameter("election_id"));
	        requestDispatcher.include(request,response);
			}
			
		else if(requestFrom.equalsIgnoreCase("Report.jsp")){
			String election_id=request.getParameter("election_id");
			ArrayList<Candidate> candidateList=new CandidateDAO().getCandidateDetailsByElectionId(Integer.parseInt(election_id));
			ArrayList<Integer> candidate=new CandidateDAO().getCandidateIdByElectionId(Integer.parseInt(election_id));
			ArrayList<Integer> report=report_dao.getResultByElectionId(Integer.parseInt(election_id),candidate);
			request.getSession().setAttribute("Candidates",candidate);
			request.getSession().setAttribute("Result",report);
			requestDispatcher=request.getRequestDispatcher("/Report.jsp?id="+request.getParameter("election_id"));
			requestDispatcher.include(request,response);
			}
		else if(requestFrom.equalsIgnoreCase("PostResult.jsp")&& "PostResult".equalsIgnoreCase(request.getParameter("action"))){
			String election_id=request.getParameter("election_id");
			ArrayList<Integer> candidate=new CandidateDAO().getCandidateIdByElectionId(Integer.parseInt(election_id));
			ArrayList<Integer> result=(ArrayList<Integer>)request.getSession().getAttribute("Result");
			boolean success=report_dao.postResult(Integer.parseInt(election_id),candidate,result);
			if(success){
				out.println("<script>alert('The Result has been posted!');</script>");
			requestDispatcher=request.getRequestDispatcher("/PostResult.jsp");
			requestDispatcher.include(request,response);
		}
			else{
				out.println("<script>alert('This result could not  be added!');</script>");
				requestDispatcher=request.getRequestDispatcher("/PostResult.jsp");
				requestDispatcher.include(request,response);
			}
			
			}

		else if(requestFrom.equalsIgnoreCase("PostResult.jsp")){
			String election_id=request.getParameter("election_id");
			ArrayList<Candidate> candidateList=new CandidateDAO().getCandidateDetailsByElectionId(Integer.parseInt(election_id));
			ArrayList<Integer> candidate=new CandidateDAO().getCandidateIdByElectionId(Integer.parseInt(election_id));
			ArrayList<Integer> report=report_dao.getResultByElectionId(Integer.parseInt(election_id),candidate);
			request.getSession().setAttribute("Candidates",candidate);
			request.getSession().setAttribute("Result",report);
			requestDispatcher=request.getRequestDispatcher("/PostResult.jsp?id="+request.getParameter("election_id"));
			requestDispatcher.include(request,response);
			}
		else if(requestFrom.equalsIgnoreCase("RemovePost_Dept.jsp")&& "RemovePost".equals(request.getParameter("action"))){
			String posts[]=request.getParameterValues("post");
			boolean success=post_dao.deletePost(posts);
			if(success){
				out.println("<script>alert('The selected post(s) has been deleted!');</script>");
				requestDispatcher=request.getRequestDispatcher("/RemovePost_Dept.jsp");
				requestDispatcher.include(request,response);
			}
			else{
				out.println("<script>alert('The selected Post(s) could not be deleted!');</script>");
				requestDispatcher=request.getRequestDispatcher("/RemovePost_Dept.jsp");
				requestDispatcher.include(request,response);
			}
			}
		else if(requestFrom.equalsIgnoreCase("RemovePost_Dept.jsp")&& "RemoveDept".equals(request.getParameter("action"))){
			String dept[]=request.getParameterValues("dept");
			boolean success=dept_dao.deleteDepartment(dept);
			if(success){
				out.println("<script>alert('The selected Department(s) has been deleted!');</script>");
				requestDispatcher=request.getRequestDispatcher("/RemovePost_Dept.jsp");
				requestDispatcher.include(request,response);
			}
			else{
				out.println("<script>alert('The selected Department(s) could not be deleted!');</script>");
				requestDispatcher=request.getRequestDispatcher("/RemovePost_Dept.jsp");
				requestDispatcher.include(request,response);
			}
			}
		else if(requestFrom.equalsIgnoreCase("RemoveCandidate.jsp")&& "RemoveCandidate".equalsIgnoreCase(request.getParameter("action"))){
			String election_id=request.getParameter("election_id");
			String candidate_id[]=request.getParameterValues("candidate_id");
			boolean success=candidate_dao.RemoveCandidate(Integer.parseInt(election_id),candidate_id);
			if(success){
				out.println("<script>alert('The selected Candidate(s) have been deleted!');</script>");
				requestDispatcher=request.getRequestDispatcher("/RemoveCandidate.jsp");
				requestDispatcher.include(request,response);
			}
			else{
				out.println("<script>alert('The selected Candidate(s) could not be deleted!');</script>");
				requestDispatcher=request.getRequestDispatcher("/RemoveCandidate.jsp");
				requestDispatcher.include(request,response);
			}
		}
		else if(requestFrom.equalsIgnoreCase("RemoveCandidate.jsp")){
			String election_id=request.getParameter("election_id");
			ArrayList<Integer> candidate=new CandidateDAO().getCandidateIdByElectionId(Integer.parseInt(election_id));
			request.getSession().setAttribute("Candidates",candidate);
			requestDispatcher=getServletContext().getRequestDispatcher("/RemoveCandidate.jsp?id="+request.getParameter("election_id"));
	        requestDispatcher.include(request,response);
			}
		

	}
	
}


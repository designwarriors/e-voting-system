package election;

import java.util.ArrayList;

public class Election {

	 String Post,Department,Year,Start_Date,End_Date,Voter_Year,Voter_Department;
	 int Election_ID=0;
	 ArrayList<Integer> candidate_ID=new ArrayList<Integer>();
	ArrayList<String> candidate_name=new ArrayList<String>();
	ArrayList<String> manifesto=new ArrayList<String>();
public int getElectionId()
{
	return Election_ID;
}
public String getPost()
{
	return Post;
}
public String getDepartment()
{
	return Department;
}
public String getYear()
{
	return Year;
}
public String getStartDate()
{
	return Start_Date;
}
public String getEndDate()
{
	return End_Date;
}
public String getVoterYear()
{
	return Voter_Year;
}
public String getVoterDepartment()
{
	return Voter_Department;
}
public ArrayList<Integer> getCandidateId()
{
	return candidate_ID;
}
public ArrayList<String> getCandidateName()
{
	return candidate_name;
}
public ArrayList<String> getManifesto()
{
	return manifesto;
}

public void setElectionId(int election_id)
{
	this.Election_ID=election_id;
}
public void setPost(String post)
{
	this.Post=post;
}
public void setDepartment(String department)
{
	this.Department=department;
}
public void setYear(String year)
{
	this.Year=year;
}
public void setStartDate(String startDate)
{
	this.Start_Date=startDate;
}
public void setEndDate(String EndDate)
{
	this.End_Date=EndDate;
}
public  void setVoterYear(String VoterYear)
{
	this.Voter_Year=VoterYear;
}
public void setVoterDepartment(String VoterDepartment)
{
	this.Voter_Department=VoterDepartment;
}
public void setCandidateId(int candidateId)
{
	this.candidate_ID.add(candidateId);
}
public void setManifesto(String manifesto)
{
	this.manifesto.add(manifesto);
}


}

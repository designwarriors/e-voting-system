package login;



import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import user.admin;
import user.user;
import election.Election;
import election.ElectionDAO;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		try{
		HttpSession userSession = request.getSession();
		String userid=request.getParameter("userid");
		String pass=request.getParameter("pass");
		String type=request.getParameter("type");
		LoginService login=new LoginService();
		if(type.equalsIgnoreCase("Student")){
		boolean authenticated=login.authenticateVoter(userid, pass);
		if(authenticated){
			user User = new user();
            User.setUserId(Integer.parseInt(userid));
            
            request.getSession().setAttribute("user", User);
			String referer="Login";
			request.getSession().setAttribute("Referer", referer);
			request.getSession().removeAttribute("Error");
			RequestDispatcher requestDispatcher=getServletContext().getRequestDispatcher("/VotingManager");
	        requestDispatcher.forward(request,response);
			
            //response.sendRedirect("voting_.jsp");
          
		}
		else{
			out.println("<script>alert('Login UnSuccessful! Invalid Credentials');</script>");
			RequestDispatcher requestDispatcher=getServletContext().getRequestDispatcher("/index.jsp");
	        requestDispatcher.include(request,response);
			
		}
	}
		else
		{
			boolean authenticated=login.authenticateAdmin(userid, pass);
			if(authenticated){
				admin Admin = new admin();
	            Admin.setAdminId(Integer.parseInt(userid));
	            System.out.println(Admin.getAdminId());
	            String referer="Login";
	            ArrayList<Election> Election=new ElectionDAO().getElection();
	            request.getSession().setAttribute("AllElection", Election);
				request.getSession().setAttribute("Admin", Admin);
				request.getSession().setAttribute("Referer", referer);
		        RequestDispatcher requestDispatcher=getServletContext().getRequestDispatcher("/ElectionManager");
		        requestDispatcher.forward(request,response);
				
		}
		else{
			
			response.sendRedirect("index.jsp");
		}
	}
		}
	 catch(Exception ex){}
}
}